import { FieldType } from '../enums/field-type'

export class Field {
  id: number;
  form_id: number;
  name: string;
  display_name: string;
  type: FieldType;
  description: string;
  required: boolean;
  pattern: string;
  display_format: string;
  order: number;
  access_level: string;
}
