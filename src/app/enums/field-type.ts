export enum FieldType {
  number = 'Number',
  string = 'String',
  html = 'Html',
  date = 'Date',
  dateRange = 'DateRange',
  select = 'Select',
  radio = 'Radio',
  checkbox = 'Checkbox',
}
