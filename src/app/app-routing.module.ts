import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsListComponent } from './views/forms-list/forms-list.component';

const routes: Routes = [
  {
    path: '',
    component: FormsListComponent,
  },
  {
    path: 'form',
    loadChildren: () =>
      import('./views/form/form-routing.module').then((m) => m.FormRoutingModule),
  },
  {
    path: 'load',
    loadChildren: () =>
      import('./views/form-loader/form-loader-routing.module').then((m) => m.FormLoaderRoutingModule),
  },
  {
    path: '**',
    redirectTo: '',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
