import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { OrderModule } from 'ngx-order-pipe';
import { DecimalPipe } from '@angular/common';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsListComponent } from './views/forms-list/forms-list.component';
import { FormComponent } from './views/form/form.component';

import { ApiService } from './services/api.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { FieldComponent } from './views/field/field.component';
import { FormLoaderComponent } from './views/form-loader/form-loader.component';
import { DisplayFormatDirective } from './directives/display-format.directive';

@NgModule({
  declarations: [
    AppComponent,
    FormsListComponent,
    FormComponent,
    FieldComponent,
    FormLoaderComponent,
    DisplayFormatDirective,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModalModule,
    OrderModule
  ],
  providers: [ApiService, DecimalPipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
