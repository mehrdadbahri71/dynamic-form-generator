import { Injectable } from '@angular/core';
import { CustomForm } from '../models/form'
import { Field } from '../models/field'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor() {}

  getForm(formId: number): Observable<CustomForm> {
    let form: CustomForm;
    let forms: CustomForm[] = JSON.parse(localStorage.getItem('forms') || '[]');
    let index: number = forms.findIndex(x => x.id == formId);
    if (index != -1) {
      form = forms[index];
    }
    return new Observable(observer => {
      observer.next(form);
      observer.complete();
    });
  }
  createForm(form: CustomForm): Observable<CustomForm> {
    let forms: CustomForm[] = JSON.parse(localStorage.getItem('forms') || '[]');
    form.id = forms.length ? forms[forms.length - 1].id + 1 : 1;
    forms.push(form);
    localStorage.setItem('forms', JSON.stringify(forms));
    return new Observable(observer => {
      observer.next(form);
      observer.complete();
    });
  }
  updateForm(form: CustomForm): Observable<CustomForm> {
    let forms: CustomForm[] = JSON.parse(localStorage.getItem('forms') || '[]');
    let index: number = forms.findIndex(x => x.id == form.id);
    if (index != -1) {
      forms[index] = form;
    }
    localStorage.setItem('forms', JSON.stringify(forms));
    return new Observable(observer => {
      observer.next(form);
      observer.complete();
    });
  }
  createUpdateFields(new_fields: Field[]): Observable<Field[]> {
    let feilds: Field[] = JSON.parse(localStorage.getItem('fields') || '[]');
    new_fields.forEach(field => {
      let index: number = feilds.findIndex(x => x.id == field.id);
      if (index != -1) {
        feilds[index] = field;
      } else {
        field.id = feilds.length ? feilds[feilds.length - 1].id + 1 : 1;
        feilds.push(field);
      }
    });
    localStorage.setItem('fields', JSON.stringify(feilds));
    return new Observable(observer => {
      observer.next(new_fields);
      observer.complete();
    });
  }
  deleteForm(formId: number): Observable<boolean> {
    let forms: CustomForm[] = JSON.parse(localStorage.getItem('forms') || '[]');
    let index: number = forms.findIndex(x => x.id == formId);
    let deleted: boolean = false;
    if (index != -1) {
      forms.splice(index, 1);
      localStorage.setItem('forms', JSON.stringify(forms));
      this.delteFormFields(formId);
      deleted = true;
    }
    return new Observable(observer => {
      observer.next(deleted);
      observer.complete();
    });
  }
  delteFormFields(formId: number): Observable<boolean> {
    let fields: Field[] = JSON.parse(localStorage.getItem('fields') || '[]');
    for(let i = fields.length - 1; i >= 0; i--) {
      if (fields[i].form_id == formId) {
        fields.splice(i, 1);
      }
    }
    localStorage.setItem('fields', JSON.stringify(fields));
    return new Observable(observer => {
      observer.next(true);
      observer.complete();
    });
  }
  getFormFields(formId: number): Observable<Field[]> {
    let fields: Field[] = JSON.parse(localStorage.getItem('fields') || '[]');
    let form_fields: Field[] = [];
    fields.forEach(field => {
      if (field.form_id == formId) {
        form_fields.push(field);
      }
    });
    return new Observable(observer => {
      observer.next(form_fields);
      observer.complete();
    });
  }
  deleteField(fieldId: number): Observable<boolean> {
    let fields: Field[] = JSON.parse(localStorage.getItem('fields') || '[]');
    let index: number = fields.findIndex(x => x.id == fieldId);
    let deleted: boolean = false;
    if (index != -1) {
      fields.splice(index, 1);
      localStorage.setItem('fields', JSON.stringify(fields));
      deleted = true;
    }
    return new Observable(observer => {
      observer.next(deleted);
      observer.complete();
    });
  }
  getField(fieldId: number): Observable<Field> {
    let fields: Field[] = JSON.parse(localStorage.getItem('fields') || '[]');
    let field: Field;
    let index: number = fields.findIndex(x => x.id == fieldId);
    if (index != -1) {
      field = fields[index];
    }
    return new Observable(observer => {
      observer.next(field);
      observer.complete();
    });
  }
  getUserAccessLevel(): Observable<number> {
    let access_level: number = JSON.parse(localStorage.getItem('access_level') || '0');
    return new Observable(observer => {
      observer.next(access_level);
      observer.complete();
    });
  }
}
