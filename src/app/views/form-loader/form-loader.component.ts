import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { CustomForm } from '../../models/form';
import { Field } from '../../models/field';
import { ApiService } from '../../services/api.service';

@Component({
  selector: 'app-form-loader',
  templateUrl: './form-loader.component.html',
  styleUrls: ['./form-loader.component.scss']
})
export class FormLoaderComponent implements OnInit {
  public formId: number;
  public form: CustomForm;
  public formFields: Field[] = [];
  public loading: boolean = true;
  public values: any = {};
  public userAccessLevel: number = 0;

  constructor(
    private route: ActivatedRoute,
    private api: ApiService,
  ) {
  }

  ngOnInit(): void {
    this.formId = parseInt(this.route.snapshot.params["id"]);
    this.getFormData();
  }
  getAccessLevel() {
    var _self = this;
    this.api.getUserAccessLevel().subscribe({
      next: (data: number) => {
        _self.userAccessLevel = data;
        _self.loading = false;
      },
      error: (err: any) => {
        console.log(err);
      }
    });
  }
  getFormData(): void {
    var _self = this;
    this.api.getForm(this.formId).subscribe({
      next: (data: CustomForm) => {
        _self.form = data;
        _self.getFormFields();
      },
      error: (err: any) => {
        console.log(err);
      }
    });
  }
  getFormFields(): void {
    var _self = this;
    this.api.getFormFields(this.formId).subscribe({
      next: (data: Field[]) => {
        _self.formFields = data;
        _self.getAccessLevel();
      },
      error: (err: any) => {
        console.log(err);
      }
    });
  }
  get_input_type(type: string): string {
    switch (type) {
      case "string":
        return "text";
      case "html":
        return "textarea";
      case "number":
        return "number";
      case "select":
        return "select";
      case "checkbox":
        return "checkbox";
      case "radio":
        return "radio";
      case "date":
        return "date";
      case "datetime":
        return "datetime";
      default:
        return "text";
    }
  }
}
