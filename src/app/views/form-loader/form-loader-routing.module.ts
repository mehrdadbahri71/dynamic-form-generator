import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FormLoaderComponent } from './form-loader.component';

const routes: Routes = [
  {
    path: ':id',
    component: FormLoaderComponent,
    data: {
      title: 'Form'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormLoaderRoutingModule {}
