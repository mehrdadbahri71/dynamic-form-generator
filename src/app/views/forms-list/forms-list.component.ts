import { Component, OnInit } from '@angular/core';
import { CustomForm } from '../../models/form'
import { ApiService } from '../../services/api.service';

@Component({
  selector: 'app-forms-list',
  templateUrl: './forms-list.component.html',
  styleUrls: ['./forms-list.component.scss']
})
export class FormsListComponent implements OnInit {
  public forms: CustomForm[];

  constructor(
    private api: ApiService,
  ) {
    this.get_data();
  }

  ngOnInit(): void {
  }

  get_data(): void {
    let forms_data = localStorage.getItem('forms');
    if (forms_data) {
      this.forms = JSON.parse(forms_data);
    }
  }
  deleteForm(form_id: number): void {
    var _self = this;
    this.api.deleteForm(form_id).subscribe({
      next: (result: boolean) => {
        if (result)
          _self.get_data();
        else
          alert("Error: couldn't delete form!");
      },
      error: (err) => {
        console.log(err);
      }
    });
  }
}
