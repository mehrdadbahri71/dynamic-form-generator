import { Component, OnInit, NgZone } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { CustomForm } from '../../models/form';
import { Field } from '../../models/field';
import { ApiService } from '../../services/api.service';
import { FieldComponent } from '../field/field.component'

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
  public formId: number;
  public form: CustomForm;
  public formFields: Field[] = [];
  public loading: boolean = true;
  public createForm: FormGroup;
  public mode: string = 'create';
  public unsaved_data: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private api: ApiService,
    private modalService: NgbModal,
    private ngZone: NgZone
  ) {
    this.createForm = new FormGroup({
      name: new FormBuilder().control('', Validators.required),
      description: new FormBuilder().control(''),
    });
  }

  ngOnInit(): void {
    let formId: string = this.route.snapshot.params["id"];
    if (formId == 'new') {
      this.formId = 0;
      this.form = new CustomForm();
      this.loading = false;
    } else {
      this.mode = 'edit';
      this.formId = parseInt(formId);
      this.getFormData();
    }
  }
  getFormData(): void {
    var _self = this;
    this.api.getForm(this.formId).subscribe({
      next: (data: CustomForm) => {
        _self.form = data;
        _self.createForm.setValue({
          name: data.name,
          description: data.description
        });
        _self.getFormFields();
      },
      error: (err: any) => {
        console.log(err);
      }
    });
  }
  getFormFields(): void {
    var _self = this;
    this.api.getFormFields(this.formId).subscribe({
      next: (data: Field[]) => {
        _self.formFields = data;
        _self.loading = false;
      },
      error: (err: any) => {
        console.log(err);
      }
    });
  }
  onSubmit(): void {
    var _self = this;
    if(!this.createForm.valid) {
      return;
    }
    this.form.name = this.createForm.value.name;
    this.form.description = this.createForm.value.description;
    if (this.mode == 'create') {
      this.api.createForm(this.form).subscribe({
        next: (data: CustomForm) => {
          _self.form = data;
          _self.formId = data.id;
          _self.formFields.forEach(element => {
            element.form_id = _self.formId;
          });
          _self.api.createUpdateFields(_self.formFields).subscribe({
            next: () => {
              _self.router.navigate(['/forms']);
            },
            error: (err: any) => {
              console.log(err);
            }
          });
        },
        error: (err: any) => {
          console.log(err);
        }
      });
    } else if(this.mode == 'edit') {
      this.api.updateForm(this.form).subscribe({
        next: () => {
          _self.api.createUpdateFields(_self.formFields).subscribe({
            next: () => {
              _self.router.navigate(['/forms']);
            },
            error: (err: any) => {
              console.log(err);
            }
          });
        },
        error: (err: any) => {
          console.log(err);
        }
      });
    }
  }
  openFieldModal(field_id: number, edit: boolean): void {
    var _self = this;
    const modalRef = this.modalService.open(FieldComponent);
    modalRef.componentInstance.fieldId = field_id;
    modalRef.componentInstance.edit = edit;
    modalRef.result.then((result) => {
      if (result) {
        if (result.id == 0) {
          let values: any = result.values;
          let new_field = new Field();
          new_field.name = values.name;
          new_field.display_name = values.display;
          new_field.description = values.description;
          new_field.type = values.type;
          new_field.required = values.required;
          new_field.pattern = values.pattern;
          new_field.display_format = values.format;
          new_field.order = values.order;
          new_field.access_level = values.access;
          if(_self.formId)
            new_field.form_id = _self.formId;
          _self.ngZone.run(() => {
            _self.formFields.push(new_field);
          });
          _self.unsaved_data = true;
        }
        else {
          let values: any = result.values;
          let field = _self.formFields.find(x => x.id == result.id);
          if(field != undefined) {
            field.name = values.name;
            field.display_name = values.display;
            field.description = values.description;
            field.type = values.type;
            field.required = values.required;
            field.pattern = values.pattern;
            field.display_format = values.format;
            field.order = values.order;
            field.access_level = values.access;
            _self.api.createUpdateFields([field]);
          }
        }
      }
    });
  }
  deleteField(field_id: number): void {
    var _self = this;
    this.api.deleteField(field_id).subscribe({
      next: () => {
        _self.getFormFields();
      },
      error: (err: any) => {
        console.log(err);
      }
    });
  }
}
