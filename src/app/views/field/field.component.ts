import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { FieldType } from '../../enums/field-type';
import { ApiService } from '../../services/api.service';

@Component({
  selector: 'app-field',
  templateUrl: './field.component.html',
  styleUrls: ['./field.component.scss']
})
export class FieldComponent implements OnInit {
  @Input() public fieldId: number;
  @Input() public edit: boolean;
  public fieldForm: FormGroup;
  public fieldTypes: any = FieldType;
  public typeKeys: string[];
  public showErrors: boolean = false;

  constructor(public activeModal: NgbActiveModal, public api: ApiService) {
    this.typeKeys = Object.keys(this.fieldTypes);
    this.fieldForm = new FormBuilder().group({
      name: ['', Validators.required],
      display: ['', Validators.required],
      type: ['string', Validators.required],
      description: ['', Validators.required],
      required: [false, Validators.required],
      pattern: [''],
      format: [''],
      order: [0, Validators.required],
      access: [0],
    });
  }

  ngOnInit(): void {
    if (this.fieldId) {
      this.getField();
    }
  }
  getField(): void {
    var _self = this;
    this.api.getField(this.fieldId).subscribe({
      next: res => {
        _self.fieldForm.setValue({
          name: res.name,
          display: res.display_name,
          type: res.type,
          description: res.description,
          required: res.required,
          pattern: res.pattern,
          format: res.display_format,
          order: res.order,
          access: res.access_level
        });
      },
      error: err => {
        console.log(err);
      }
    });
  }
  onSubmit() {
    if (this.fieldForm.valid) {
      let data: any = {
        id: this.fieldId,
        values: this.fieldForm.value
      }
      this.activeModal.close(data);
    } else {
      this.showErrors = true;
    }
  }

}
