import { Directive, OnInit, Input, Output, HostBinding, HostListener } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { DecimalPipe } from '@angular/common';

@Directive({
  selector: '[appDisplayFormat]'
})
export class DisplayFormatDirective implements OnInit {
  @Input() appDisplayFormat: string;
  @Input() format: string;
  @HostBinding('value') stringValue: string;
  @Output() appDisplayFormatChange: EventEmitter<string> = new EventEmitter();

  constructor(private decimalPipe: DecimalPipe) { }

  ngOnInit() {
    let formatted = this.decimalPipe.transform(this.appDisplayFormat, this.format);
    if(formatted != null && formatted != undefined)
      this.stringValue = formatted;
    else
      this.stringValue = '';
  }
  @HostListener('blur', ['$event.target.value'])
  @HostListener('keyup.enter', ['$event.target.value'])
  formatANumber(value: any) {
    console.log(value);
    const numberValue = parseFloat(value.replaceAll(',', ''));
    console.log(numberValue);
    let formatted = this.decimalPipe.transform(numberValue, this.format);
    console.log(formatted);
    if (formatted)
      this.stringValue = formatted;
    this.appDisplayFormatChange.next(numberValue.toString());

  }
}
